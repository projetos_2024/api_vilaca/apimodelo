const router = require("express").Router();
const dbController = require("../controller/dbController"); 

router.get("/tables/", dbController.getTables);

module.exports = router;
