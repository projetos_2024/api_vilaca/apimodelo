const connect = require("../db/connect");

module.exports = class dbController {
  static async getTables(req, res) {
    // consulta para obter a lista de tabelas
    const queryShowTables = "show tables";

    connect.query(queryShowTables, async function (err, result, fields) {
      if (err) {
        console.log(err);
        return res
          .status(500)
          .json({ error: "Erro ao obter tabelas do banco de dados" });
      } //fim do if

      const tableNames = result.map((row) => row[fields[0].name]);
      // Extrai os nomes das tabelas
      //res.status(200).json({message: "Tabelas do banco - forma bruta:", result,tables: tableNames});
      console.log("Tabelas do banco de dados:", tableNames);
      //return res.status(200).json({ message: "Tabelas do banco", tables: tableNames});

      //organização e descrição das tabelas do banco
      const tables = [];

      //iterar sobre os resultados para obter a descrição de cada tabela
      for (let i = 0; i < result.length; i++) {
        //analisando o banco através de seus atributos
        const tableName =
          result[i][`Tables_in_${connect.config.connectionConfig.database}`];

        //acionando o comando desc
        const queryDescTable = `describe ${tableName}`;

        try {
          const tableDescription = await new Promise((resolve, reject) => {
            connect.query(queryDescTable, function (err, result, fields) {
              if (err) {
                reject(err);
              } //fim do if
              resolve(result);
            }); //fim do connect
          }); //fim da const
          tables.push({ name: tableName, description: tableDescription });
        } catch (error) {
          console.log(error);
          return res.status(500).json({ error: "Erro ao obter a descrição!" });
        } //fim do catch
      } //fim do for

      res
        .status(200)
        .json({
          message: "Obtendo todas as tabelas de suas descrições",
          tables,
        });
    }); //fechamento connect query
  } //fim do get tables
};
